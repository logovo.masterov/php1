<?php
include_once "userFuncs.php";
if (!empty($_POST)) {
    $user = new User($_POST['login'] ?? '', $_POST['password'] ?? '');

    if (!$user->autentificate()) {
        $errorMessage = $user->validate();

    } else {
        session_start();
        $_SESSION['login'] = $user->getLogin();
        header('Location: welcome.php');
    }
}
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link href="styles.css?v=1" rel="stylesheet" type="text/css">
</head>
<body>

<form action="login.php" method="post">
    <div class="row">
        <label>Login</label><input type="text" name="login" value="<?= $_POST['login'] ?? '' ?>">
    </div>
    <div class="error">
        <?php
            //echo $user->getError("log");

        ?>
    </div>
    <div class="row">
        <label>Password</label><input type="password" name="password">
    </div>
    <div class="error">
        <?php
        //echo $user->getError("pass");
        ?>
    </div>
    <div>
        <button type="submit">Login</button>
    </div>


</form>

<a href="registration.php">Register</a>
</body>
</html>
