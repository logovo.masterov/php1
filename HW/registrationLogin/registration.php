<?php
include_once "userFuncs.php";
$user = new User($_POST['login'] ?? '', $_POST['password'] ?? '');

if (!empty($_POST) && $user->validate()) {
    $user->save();
}
?>
<!doctype html>
<html lang="ru" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration</title>
    <link href="styles.css" rel="stylesheet" type="text/css">
</head>
<body>

<form action="registration.php" method="post">
    <div class="row">
        <label>Login</label><input type="text" name="login" value="<?= $_POST['login'] ?? '' ?>">
    </div>
    <div class="error">
        <?php
        echo $user->getError("log");
        ?>
    </div>
    <div class=" row">
        <label>Password</label><input type="text" name="password">
    </div>
    <div class="error">
        <?php
        echo $user->getError("pass");
        ?>
    </div>
    <button type="submit">Registration</button>

</form>
<a href="login.php">Login</a>
</body>
</html>

