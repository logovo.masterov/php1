<?php

class User
{
    private $login;
    private $password;
    private $errors;
    private $users;

    public function autentificate(): bool
    {
        $logins = $this->getLogins();
        $passwords = $this->getPasswords();

        $loginIndex = -1;
        foreach ($logins as $k => $l) {
            if ($l == $this->login) {
                $loginIndex = $k;
                break;
            }
        }

        if (-1 == $loginIndex) {
            return false;
        }

        return password_verify($this->password, $passwords[$loginIndex]);
    }

    public function __construct(string $login, string $password)
    {
        $this->login = trim($login);
        $this->password = trim($password);
        $this->errors = [];
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    private function getLogins(): array
    {
        $result = [];

        $filePath = __DIR__ . '/dataLog.txt';
        $lines = file_get_contents($filePath);
        if (false === $lines) {
            return $result;
        }

        $logins = explode("\n", $lines);
        $result = $logins;

        return $result;

    }

    private function getPasswords()
    {
        $result = [];
        $filePath = __DIR__ . '/dataPass.txt';
        $lines = file_get_contents($filePath);
        if (false === $lines) {
            return $result;
        }
        $passwords = explode("\n", $lines);
        $result = $passwords;
        return $result;

    }

    private function checkLogin(): bool
    {
        $logins = $this->getLogins();

        return in_array($this->login, $logins);
    }

    private function saveLogin()
    {

        $filePath = __DIR__ . '/dataLog.txt';

        $file = fopen($filePath, 'a');
        fwrite($file, "$this->login\n");
        fclose($file);
    }

    private function saveHashes()
    {

        $passwordHash = password_hash($this->password, PASSWORD_DEFAULT);
        $filePath = __DIR__ . '/dataPass.txt';

        $file = fopen($filePath, 'a');
        fwrite($file, "$passwordHash\n");
        fclose($file);
    }

    public function save()
    {
        $this->saveLogin();
        $this->saveHashes();
    }

    public function validate(): bool
    {
        if ($this->login == '') {
            $this->errors["log"] = "Логин не можжет быть пустым";
        }

        if ($this->password == '') {
            $this->errors["pass"] = "Пароль не может быть пустым";
        }

        if ($this->checkLogin()) {
            $this->errors["log"] = "Логин уже существует";
        }

        return count($this->errors) == 0;
    }

    public function getError(string $name): string
    {

        if (isset($this->errors[$name])) {
            return $this->errors[$name];
        }

        return "";


        //return isset($this->errors[$name]) ? $this->errors[$name] : '';

        //return $this->errors[$name] ?? '';


    }

    public function readListUsers()
    {
        echo pathinfo(__DIR__);
    }
}